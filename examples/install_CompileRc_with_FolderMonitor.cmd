@echo off
:: ����������� � ����� � ��������
:: ��������� � ����������: ��� ������� � �����������, ��� ����

chcp 1251
echo �������� ���������� �������
if a%1==a%1%1 (
	echo ��������� � ����������: ��� ������� � �����������, ��� ����
	pause
	exit
	)
SET dpr=%1


echo - ����������� ������� ����������� ������������
echo   � ����������� �� ����� %%PATH%%
echo - git
where git || goto ERR
echo - DCC32 (���������� Delphi ��� ��������� ������, ������ ��� ���������)
where DCC32 || goto ERR
echo - BRCC32 (���������� �������� Delphi ��� ��������� ������)
where BRCC32 || goto ERR
echo - sed (������ ��� ���������)
where sed || goto ERR
echo - touch
where touch || goto ERR
echo - sleep
where sleep || goto ERR

echo - ����������� ������� git �����������
git status 2>nul 1>nul || goto ERR
echo - ������ git ����������� (���������� ������������� ���������)
git status -s | grep -v %~nx0 1>nul 2>nul && (
	echo.
	echo =======================================================
	git status -sb | grep -v %~nx0
	echo =======================================================
	echo !!! � git ���� ������������� ��������� ������ ������� ����� !!!
	echo ���������� ������� ������ ��� �������� ���������.
	pause
	exit
)

echo - �������� ��������� �����
md RES
md RES\SRC
cd RES || goto ERR

echo - �������� ����� git
git branch feature/CompileRc
git checkout feature/CompileRc || goto ERR

echo - �������� ��������� CompileRc
git submodule add https://bitbucket.org/danik-ik/compilerc CompileRc && ^
echo - ��������� �� ��������� dpr && ^
sed -b s/MyProject.dpr/%dpr%/ CompileRc\examples\AutoCompileRc.Config.cmd > AutoCompileRc.Config.cmd && ^
echo - �������� �� ���������� && ^
copy CompileRc\examples\encrypt.cmd && ^
echo - ���������� ������� && ^
type CompileRc\examples\.gitignore >> .gitignore || goto ERR
git add * && git commit -am"CompileRc" || goto ERR

echo - �������� ��������� � ���������� FolderMonitor
git submodule add https://bitbucket.org/danik-ik/foldermonitor FolderMonitor && ^
cd FolderMonitor && ^
git submodule update --init --recursive && ^
DCC32.exe FolderMonitor.dpr -B && ^
cd .. || goto ERR

echo - �������������� �������� �������� && ^
copy "CompileRc\examples\FolderMonitor\_�������������� ��������.cmd" || GOTO ERR
git add * && git commit -am"FolderMonitor" || goto ERR
cd .. || GOTO ERR

echo - ���������� ����� �� �������� � ������
copy %dpr% %dpr%.bak && ^
call res\CompileRc\ReplaceBetween.cmd %dpr%.bak res/CompileRc/examples/insert_into_dpr_BEFORE_uses.pas ; \r\n > %dpr% && ^
git add %dpr% && git commit -am"dpr header" && ^
goto OK || ^
goto ERR

exit

:OK
echo ��������� ��������� �������!
echo ������ ���� ��� �� ����� � ������ ����� �����.
echo ��� ������� ������ ����� ������� ����� (feature/CompileRc) � �������.
pause    
del %0
exit

:ERR
echo ERROR
pause
