const { watch, series } = require('gulp');
const spawn = require('child_process').spawn;

function compileResources(cb) {
  var cmd = spawn('CompileRc\\CompileAllResources.cmd', [], {stdio: 'inherit'});
  cmd.on('close', function (code) {
    cb(code);
  });
}

exports.default = function() {
  compileResources(code=>{})
  watch('Src/**', compileResources); // series(compileResources, ...)
};