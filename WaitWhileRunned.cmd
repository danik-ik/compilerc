@Echo off
:loop
for /f %%i in ('call %~dp0ProcExists.cmd %1') do (
	echo [%time%] Waiting for the "%1" application
	sleep 2
	goto loop
)
